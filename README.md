BitBurn
=======

BitBurn is a burndown chart generator that feeds from BitBucket issue tracker
by interpreting metadata added in issue titles.  It's Free Software released
under the [GPL]. It was developed by [FFIT Serviços de Inovação e Tecnologia].

It's a experimental software on *alpha* stage.  It's very buggy and unstable: a
quick-and-dirty hack wrapped up in a few days.  Feel free to refactor it and
submit patches.


Screenshot
----------

Sample burndown generated with bitburn:

![BitBurn Screenshot](https://ffit.com.br/bitburn/sample-sm.png)

It has the unresolved effort in dark blue and new effort (that is not open yet)
in light blue.


Usage
-----

To use it you must add metadata to the issues in your sprint, set up the
reference date and days of the sprint, then run several calls to bitburn during
the sprint. *Each call will plot a point in the graph.* The points will be
stored in ``~/.cache/bitburn``.

The issues will be filtered by milestone, which you can add by creating some
milestones on your BitBucket project settings and setting the milestones in
your issues.  The number of points of each issue can be set on their titles
with the format [XXXp].  You can also code the points as a component in the
format NUMBERp: ``/p``, ``1p``, ``2p``, ``3p``.  Example:

| Title                              | T | P | Status | Assignee | Milestone
||||||-
| Implement user authentication [3p] | . | . | new    | unssngnd | sprint1
| [1p] Layout of login screen        | . | . | open   | someone  | sprint1
| [7p] User CRUD                     | . | . | resolv | someone  | sprint1
| Optimize queries ``1p``            | . | . | new    | unssngnd | sprint2
| [13p] Install deployment server    | . | . | new    | unssngnd | sprint2
| [5p] User password reset           | . | . | new    | unssngnd | sprint2

[BitBurn issue tracker](https://bitbucket.org/ffit/bitburn/issues?sort=-priority)
provides more examples on this.

On the first call, it might be necessary to authenticate (for projects that are
not public), use the ``--user`` and ``--password`` parameters for that.  In the
subsequent calls you'll have to provide four parameters and a command:

  * -p / --project  owner/repo
  * -m / --milestone identifier or -s / --sprint   id
  * -r / --reference-date yyyy-mm-dd
  * -d / --days     n,n,n,n,n,n,n

Check ``bitburn --help`` for more details.

Commands:

  * summary: shows a summary of the issues in the repo, you might start with
    this one to test connectivity and parsing
  * plot: plots a graph in *pyxplot* format
  * plotpoint: adds a point to the list without plotting the graph
  * rmcache: removes cache and sprint history from the project and milestone passed
  * rmauth: resets authentication for that repository


Examples
--------

on the first call, it might be needed to pass two extra parameters for user authentication:

	bitburn  --user username  --password password


The example below shows a summary of issues on repository ``owner/repo`` that are part
of sprint 3.  The reference date for the sprint is April 30th.  And the days
are 1, 2, 3, 6, 7, 8, 9, 10 from that reference date.  That means March 1st,
2nd, 3th, 6, 7, 8, 9, 10.

	bitburn  -p owner/repo  -s 3  -r 2013-04-30  -d 1,2,3,6,7,8,9,10  summary


The example below plots a graph from repository ``test/sandbox`` taking account
the issues that are part of sprint 13.  The reference date is March 6.  That
day, and the three subsequent days are part of the sprint.

	bitburn  -p test/sandbox  -m sprint13  --reference-date 2013-03-06 -d 0,1,2,3  plot  |  pyxplot


The reference date and days list were added to account for weekends and
holydays.  For a sprint starting on Thursday and ending on Tuesday, you'll have
a "hole" in the days list:

	bitburn  -p some/prj  -m "Sprint 7"  -r 2013-05-02  --days 0,1,4,5  plot


It is possible to have sprints with issues scattered across multiple projects,
in that case, you can use a comma (``,``) to specify that:

    bitburn  -p some/prj,other/project  -m sprint2  summary


Dependencies
------------

BitBurn only works on Ruby > 1.9. You'll also need:

* libcurl development headers: ``apt-get install libcurl4-openssl-dev`` on
  Ubuntu will do the trick
* curb and json gems: ``gem install curb json``
* pyxplot: for plotting the graphs, ``sudo apt-get install pyxplot`` on Ubuntu


[GPL]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[FFIT Serviços de Inovação e Tecnologia]: https://ffit.com.br/
